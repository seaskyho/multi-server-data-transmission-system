# High Availability and Eventual Consistency Project

### Report
This project is divided into two stages. The code in this repository shows the final system after stage 2. However, there are still two reports (**Report part1.pdf** and **Report part2.pdf**), written after each stage. To have a better understanding of the project, you are welcome to read them.
### Usage Instruction
To build and generate jar files, run the following command in the root directory of this project.
```mvn package```. The generated jar files will be in /target directory.

The executable for the server is ``Server-jar-with-dependencies.jar`` 

The executable for the client is ``Client-jar-with-dependencies.jar``

For args info, use ``-h`` arg for each executable.

The key args of the server are: 
* ``-ms`` stands for **my secret** is set to indicate the server's own secret
* ``-s`` stands for **secret** is set to indicate the secret of the server to connect to
* ``-a`` is set to indicate the time interval (in milliseconds) for activity message buffer to send and reset

Other args are exactly like the ones in the first project.